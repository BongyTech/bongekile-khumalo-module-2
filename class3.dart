class App {

    String name_of_app,category,developer,year;
   
    App(this.name_of_app, this.category, this.developer, this.year); 

    String sentence() {
        return "Name: ${this.name_of_app}\n Category: ${this.category}\n Developer: ${this.developer}\n Year: ${this.year}";
    }    

    String sentence_UPPERCASE() {
        return "Name: ${this.name_of_app}\n Category: ${this.category}\n Developer: ${this.developer}\n Year: ${this.year}"
          .toUpperCase();
    }
    
}

main() {

    var app = App("FNB", "Best Consumer Solution", "Developer", "2012"); 
       
    // --- Camel case ---
    print(app.sentence());

    // --- Capital Letters ---
    print(app.sentence_UPPERCASE());

}
